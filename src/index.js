import './ui/assets/css/style.scss';
import page from './data.json'
import React from 'react';
import ReactDOM from 'react-dom/client';
import {Page} from './ui.jsx';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <React.StrictMode>
        <Page page={page}/>
    </React.StrictMode>
);
