import {Button, Card, Stack} from "react-bootstrap";

const ContentFooter = () => {
    return (
        <Card.Footer>
            <Stack direction="horizontal" gap={3}>
                <div className="main-content-footer">
                    <Button variant="dark-green" type="button">
                        Add a subscription
                    </Button>
                </div>
            </Stack>
        </Card.Footer>
    );
};


export default ContentFooter;