import {Table} from 'react-bootstrap';
import ellipse from '../../assets/svg/grid-ellipse.svg';
import sidebar from "../../assets/svg/sidebar.svg";

const Grid = ({data}) => {
    return (
        <Table className="content-grid" responsive borderless>
            <thead>
            <tr>
                {data.metadata.headers.map((header, index) => (
                    <th key={index}>{header.label && header.label}
                        {header.sort &&
                            <svg className="icon grid-sort">
                                <use xlinkHref={`${sidebar}#arrow`}/>
                            </svg>
                        }
                    </th>
                ))}
            </tr>
            </thead>
            <tbody>
            {data.payload.map((promo, index) => (
                <tr className="justify-content-between" key={index}>
                    <td>
                        <svg className="icon filter">
                            <use xlinkHref={`${sidebar}#grid-chevron-selector-vertical`}/>
                        </svg>
                    </td>
                    <td>{promo.subscriptionName}</td>
                    <td>{promo.discountType}</td>
                    <td>{promo.discountOff}</td>
                    <td>{promo.endDate}</td>
                    <td><span
                        className={promo.product === "Course" ? "btn btn-light-blue btn-sm" : "btn btn-light-red btn-sm"}>
                      {promo.product}
                  </span>
                    </td>
                    <td>
                      <span
                          className={(promo.status === "Active" ? "btn btn-light-green btn-sm" : "btn btn-light-yellow btn-sm")
                              + "justify-content-between"}>
                      <svg className="icon grid-ellipse">
                        <use xlinkHref={`${sidebar}#status`}/>
                      </svg>
                      <span>{promo.status}</span>
                  </span>
                    </td>
                    <td>
                        <img className="icon grid-ellipsis" alt="options tooltip" src={`${ellipse}`}/>
                    </td>
                </tr>
            ))}

            </tbody>
        </Table>
    );
};

export default Grid;
