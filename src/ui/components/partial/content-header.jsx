import {Card, Stack} from "react-bootstrap";
import AdminBreadcrumb from './breadcrumb';

const ContentHeader = ({title, crumbs}) => {
    return (
        <Stack gap={2}>
            <Card.Title as="h4">{title}</Card.Title>
            <AdminBreadcrumb crumbs={crumbs}/>
        </Stack>
    );
};
export default ContentHeader;