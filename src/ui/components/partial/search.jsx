import {Button} from 'react-bootstrap';
import sidebar from "../../assets/svg/sidebar.svg";

const Search = ({menu}) => {
    return (
        <Button className="sidebar-search d-flex" variant="light" type="button">
            <div className="p-1">
                <svg className="icon">
                    <use xlinkHref={`${sidebar}#search`}/>
                </svg>
            </div>
            <div className="p-1 flex-grow-1 align-self-start">Search</div>
        </Button>
    );
};

export default Search;
