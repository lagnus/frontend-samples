import { Card, Stack} from 'react-bootstrap';
import AdminNavbar from './navbar';
import Search from './search';
import sidebar from '../../assets/svg/sidebar.svg';
import logo from '../../assets/svg/logo.svg';

const AdminSidebar = ({user, settings, notes, menu}) => {

    return (
        <Stack className="sidebar">
            <Card className="sidebar-menu">
                <Stack>
                    <div className="d-flex">
                        <div className="p-2 flex-grow-1">
                            <svg className="logo">
                                <use xlinkHref={`${logo}#logo`}/>
                            </svg>
                        </div>
                        <div className="sidebar-collapse btn p-2" >
                            <svg className="icon">
                                <use xlinkHref={`${sidebar}#sidebar-collapse`}/>
                            </svg>
                        </div>
                    </div>
                    <Search/>
                    <AdminNavbar menu={menu}/>
                </Stack>
            </Card>
            <Card className="sidebar-util justify-content-end">
                <div className="placeholder-util btn bg-white col-md-10 mx-auto my-auto"/>
                <AdminNavbar className="admin-settings" menu={settings}/>
                <div className="user-util d-flex">
                    <div className="p-2">
                        <svg className="icon">
                            <use xlinkHref={`${sidebar}#user`}/>
                        </svg>
                    </div>
                    <div className="p-2 flex-grow-1">User name</div>
                    <div className="p-2">
                        <svg className="icon">
                            <use xlinkHref={`${sidebar}#ellipse`}/>
                        </svg>
                    </div>
                </div>
            </Card>
        </Stack>
    );
};

export default AdminSidebar;
