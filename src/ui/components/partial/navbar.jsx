import {Nav} from 'react-bootstrap';
import sprite from '../../assets/svg/sidebar.svg';

const AdminNavbar = ({menu}) => {
    return (
        <Nav defaultActiveKey="/dashboard" className="sidebar-nav-menu flex-column">
            {menu.map((navItem, index) => (
                <Nav.Item key={index} className={`${navItem.id}`}>
                    <Nav.Link eventKey={navItem.eventKey}>
                        <svg className="icon">
                            <use xlinkHref={`${sprite}#${navItem.id}`}/>
                        </svg>
                        {navItem.label}
                    </Nav.Link>
                </Nav.Item>
            ))}
        </Nav>
    );
};

export default AdminNavbar;
