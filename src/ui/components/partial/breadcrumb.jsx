import {Breadcrumb, Card} from 'react-bootstrap';

const AdminBreadcrumb = ({crumbs}) => {
    return (
        <Card.Subtitle as="h6">
            <Breadcrumb>
                {crumbs.map((crumb, index) => (
                    <Breadcrumb.Item key={index}>
                        {crumb.label}
                    </Breadcrumb.Item>
                ))}
            </Breadcrumb>
        </Card.Subtitle>
    );
};

export default AdminBreadcrumb;
