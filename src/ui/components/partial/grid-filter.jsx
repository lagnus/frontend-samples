import {Button, Stack} from "react-bootstrap";
import gridsvg from "../../assets/svg/grid.svg";

const GridFilter = () => {
    return (
        <Stack className="content-grid-filter justify-content-end" direction="horizontal" gap={2}>
            <Stack className="btn btn-dark-gray-compound" direction="horizontal" gap={1}>
                <Button variant="dark-gray" type="button">
                    <svg className="icon filter">
                        <use xlinkHref={`${gridsvg}#filter-search`}/>
                    </svg>
                </Button>
                <Button variant="dark-gray" type="button">
                    <svg className="icon filter">
                        <use xlinkHref={`${gridsvg}#filter-lines`}/>
                    </svg>
                </Button>
            </Stack>
            <Button className="filter-switch" variant="dark-gray" type="submit">
                <svg className="icon filter">
                    <use xlinkHref={`${gridsvg}#filter-switch`}/>
                </svg>
            </Button>
        </Stack>
    );
};
export default GridFilter;