import {Button, Col, Container, Dropdown, Row, Stack} from "react-bootstrap";
import AdminSidebar from "./partial/sidebar";
import ContentHeader from "./partial/content-header";
import sidebar from "../assets/svg/sidebar.svg";
import {AdminPromo} from "../../ui";
import React from "react";

function Page({page}) {
    return (
        <Container className="page" fluid="xxl">
            <Row>
                <Col xs={5} sm={4} md={2} xl={3} className="page-sidebar"><AdminSidebar menu={page.menu}
                                                                                        settings={page.settings}/></Col>
                <Col xs={7} sm={8} md={10} xl={9} className="page-content">
                    <Stack gap={5}>
                        <Stack direction="horizontal" gap={5}>
                            <Button className="justify-content-between" variant="dark-green" type="button">
                                <span> Web app </span>
                                <svg className="icon page-www">
                                    <use xlinkHref={`${sidebar}#www`}/>
                                </svg>
                            </Button>
                            <Dropdown>
                                <Dropdown.Toggle id="app-valid-menu" variant="outline-custom-dark">
                                    Application Validation
                                    <svg className="icon">
                                        <use xlinkHref={`${sidebar}#chevron`}/>
                                    </svg>
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Tea</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Time 6pm</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Scones</Dropdown.Item>
                                    <Dropdown.Item href="#/action-4">Hot Cross buns</Dropdown.Item>
                                    <Dropdown.Item href="#/action-5">Clotted Cream</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Stack>
                        <ContentHeader title={page.title} crumbs={page.crumbs}/>
                    </Stack>
                    <Stack gap={4}>
                        <AdminPromo records={page.data}/>
                        <Stack direction="horizontal" gap={5}>
                            <Button variant="dark-green" type="button">
                                <span> Add a subscription </span>
                            </Button>
                        </Stack>
                    </Stack>
                </Col>
            </Row>
        </Container>
    );
}

export default Page;
