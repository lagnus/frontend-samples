import Grid from "../partial/grid";
import AdminGridFilter from "../partial/grid-filter";
import {Card} from "react-bootstrap";

const AdminPromo = ({records}) => {
    return (
        <Card>
            <Card.Header>
                <Card.Title>
                    <AdminGridFilter/>
                </Card.Title>
            </Card.Header>
            <Card.Body>
                <Grid data={records}/>
            </Card.Body>
        </Card>
    );
};

export default AdminPromo;
