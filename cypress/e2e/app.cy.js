describe('App', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('renders correctly', () => {
    cy.get('html').should('exist');
    cy.get('head').should('exist');
    cy.get('script[defer]').should('exist');
    cy.get('meta[name="viewport"]').should('exist');
    cy.get('.app').should('exist');
    cy.get('.sidebar').should('exist');
    cy.get('.main-content').should('exist');
    cy.get('.navbar').should('exist');
    cy.get('.breadcrumb').should('exist');
    cy.get('.promo-grid').should('exist');
  });

  it('is responsive', () => {
    cy.viewport('iphone-6');
    cy.get('.sidebar').should('not.be.visible');
    cy.get('.main-content').should('have.css', 'padding', '0.5rem');

    cy.viewport('ipad-2');
    cy.get('.sidebar').should('be.visible');
    cy.get('.main-content').should('have.css', 'padding', '1rem');

    cy.viewport('macbook-15');
    cy.get('.sidebar').should('be.visible');
    cy.get('.main-content').should('have.css', 'padding', '1rem');
  });
});
