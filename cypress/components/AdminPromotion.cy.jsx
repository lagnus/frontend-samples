import React from 'react';
import App from '../../src/components/App';

describe('App', () => {
  it('renders correctly', () => {
    cy.mount(<App />);
    cy.get('.app').should('exist');
    cy.get('.sidebar').should('exist');
    cy.get('.main-content').should('exist');
    cy.get('.navbar').should('exist');
    cy.get('.breadcrumb').should('exist');
    cy.get('.promo-grid').should('exist');
  });
});
