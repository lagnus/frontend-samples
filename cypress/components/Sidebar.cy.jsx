import React from 'react';
import Sidebar from '../../src/components/Sidebar';

describe('Sidebar', () => {
  it('renders correctly', () => {
    cy.mount(<Sidebar />);
    cy.get('.sidebar').should('exist');
    cy.get('.nav-link').should('have.length.greaterThan', 0);
    cy.get('.nav-link .icon').should('exist');
  });
});
