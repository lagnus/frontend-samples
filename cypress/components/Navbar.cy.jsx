import React from 'react';
import Navbar from '../../src/components/Navbar';

describe('Navbar', () => {
  it('renders correctly', () => {
    cy.mount(<Navbar />);
    cy.get('.navbar').should('exist');
    cy.get('.navbar-brand').should('exist');
    cy.get('.form-control').should('exist');
    cy.get('.dropdown-toggle').should('exist');
  });
});
