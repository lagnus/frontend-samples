import React from 'react';
import PromoGrid from '../../src/components/PromoGrid';

describe('AdminGrid', () => {
  it('renders correctly', () => {
    cy.mount(<AdminGrid />);
    cy.get('.admin-grid').should('exist');
    cy.get('table').should('exist');
    cy.get('thead th').should('have.length.greaterThan', 0);
    cy.get('tbody tr').should('have.length.greaterThan', 0);
    cy.get('.btn').should('exist');
  });
});
