import React from 'react';
import Breadcrumb from '../../src/components/Breadcrumb';

describe('Breadcrumb', () => {
  it('renders correctly', () => {
    cy.mount(<Breadcrumb />);
    cy.get('.breadcrumb').should('exist');
    cy.get('.breadcrumb-item').should('have.length.greaterThan', 0);
    cy.get('.btn').should('exist');
  });
});
